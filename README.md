# Thème Yakuake - Breeze Greeny Dark

Fichiers de configuration du thème Yakuake Breeze Greeny Dark.

Procédure d'installation :
- créer une tarball contenant les fichiers de configuration : `tar -czvf breeze_greeny_dark.tar.gz <dossier>`
- installer la tarball dans Yakuake

Ajout du fichier `yakuakerc` dans le dossier `~/.config/`.

Contenu de ce fichier :
```
[Animation]
Frames=12

[Appearance]
Skin=breeze_greeny_dark

[Desktop Entry]
DefaultProfile=Greeny Dark.profile

[Dialogs]
FirstRun=false

[KFileDialog Settings]
Recent Files[$e]=breeze-greeny-black,file:$HOME/.config/breeze_greeny_dark.tar.gz
Recent URLs[$e]=file:$HOME/Workspace/,file:$HOME/
detailViewIconSize=16

[Window]
Height=79
```
